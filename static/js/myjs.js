//var btn_send = document.getElementById("btn-send");
//btn_send.onclick = function(){
//     document.getElementById('rgemail-input').style.display = "block";
//     document.getElementById('ask_for_code').value="1";
//     return true;
//}

function check_register() {
    var pwd1 = document.getElementById("rgpassword").value;
    var pwd2 = document.getElementById("rgrepeat_password").value;
    var username = document.getElementById("rgusername").value;
    var email = document.getElementById("rgemail").value;
    var regEmail = /^[a-zA-Z\d]+([-_\.][a-zA-Z\d]+)*@[a-zA-Z\d]+\.[a-zA-Z\d]{2,4}$/;
    var validated = true;
    if(pwd1 == null || pwd2 == null || username == null || email == null) {
        document.getElementById("warning-info").style.display = "block";
        validated = false;
    }
    if(pwd1 != pwd2){
        document.getElementById("rgpwd_alert").style.display = "block";
        validated = false;
    }
    if(pwd1.length<8 || pwd2.length<8) {
        document.getElementById("rgpwd_alert_2").style.display = "block";
        validated = false;
    }
    if(email != "" && (!regEmail.test(email))) {
        document.getElementById("rgemail_alert").style.display = "block";
        validated = false;
    }
    if((!document.getElementById('rgmale').checked)&&(!document.getElementById('rgfemale').checked)) {
        document.getElementById("rgsex_alert").style.display = "block";
        validated = false;
    }
    return validated;
}


function check_add_product() {
    var name = document.getElementById("pdt-name").value;
    var price = document.getElementById("pdt-price").value;
    var cate = document.getElementById("pdt-category").value;
    var img = document.getElementById("pdt-image").value;
    var validated = true;

    if(name == "" || price == "") {
        document.getElementById("alert-vide").style.display = "block";
        validated = false;
    }

    // check image format
//    var imgOK = /^[\u4E00-\u9FA5A-Za-z0-9_]*.(gif|jpg|jpeg|png|gif|jpg|png)$/;
    var imgOK = /\.(gif|jpg|jpeg|png|GIF|JPG|PNG)$/;
    if(img != "" && (!imgOK.test(img))) {
        document.getElementById("alert-image").style.display = "block";
        validated = false;
    } else if (img == "" ) {
        validated = true;
    }

    // check price
    var priceok = /^[0-9]+([.]{1}[0-9]+){0,3}$/
    if(price != null && (!priceok.test(price)))
    {
        document.getElementById("alert-price").style.display = "block";
        validated = false;
    }
    return validated;
}

function check_mdf_pwd() {
    var pwd1 = document.getElementById("new-pwd").value;
    var pwd2 = document.getElementById("re-pwd").value;
    var validated = true;
    if(pwd1 != pwd2){
        document.getElementById("alert").style.display = "block";
        validated = false;
    }
    if(pwd1.length<8 || pwd2.length<8) {
        document.getElementById("alert_2").style.display = "block";
        validated = false;
    }
    return validated;
}

function get_lid() {
    like = document.getElementById("like_id").value;
    document.cookie = "like_id"+ "=" + like + "; expires=" + date.toGMTString();
    return true;
}


