import logging
from sqlite3 import DatabaseError
from django.views.decorators.http import require_http_methods, require_GET,require_POST
from django.http import HttpResponse
from django.shortcuts import render,redirect
from django.views import generic
import re
from apps.favorite.models import *
from apps.orders.models import *
from .forms import *
from django.db.models import Q
import random


class IndexView(generic.ListView):
    model = Product
    template_name = 'index.html'
    context_object_name = 'all'
    paginate_by = 9
    username = ""
    user = None

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(IndexView, self).get_context_data(*kwargs)
        paginator = context.get('paginator')
        page_obj = context.get('page_obj')
        pagination_data = self.get_pagination_data(paginator, page_obj)
        context.update(pagination_data)
        try :
            self.username = self.request.session['username']
            self.user = User.objects.get(username=self.username)
            context.update({'username':self.username})
        except KeyError as e:
            print("Nobody has been login.")
        return context

    def get_queryset(self):
        products = Product.objects.filter(is_available=True).order_by('-date_post')
        return products


    def get_pagination_data(self, paginator, page_obj, around_count=2):
        currentpage = page_obj.number
        totalpage = paginator.num_pages
        left_has_more = False
        right_has_more = False
        if currentpage <= around_count + 2:
            leftpage = range(1, currentpage)
        else:
            left_has_more = True
            leftpage = range(currentpage - around_count, currentpage)

        if currentpage >= totalpage - around_count - 1:
            rightpage = range(currentpage + 1, totalpage)
        else:
            right_has_more = True
            rightpage = range(currentpage + 1, currentpage + around_count+1)

        return {
            'current_page' : currentpage,
            'left_pages': leftpage,
            'right_pages': rightpage,
            'left_has_more':left_has_more,
            'right_has_more':right_has_more,
            'total_page' :totalpage,
            'paginate_by':self.paginate_by,
        }


def handle_uploaded_file(f):
    with open('some/file/name.txt', 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

@require_http_methods(['POST','GET'])
def add_product(request):
    context = {}
    if request.method == "GET":
        pass
    if request.method == "POST":
        flag = 0
        username = request.session['username']
        user = User.objects.get(username=username)
        price = request.POST.get('pdt-price')
        priceOK =  '^[0-9]+(.[0-9]{1,3})?$'
        name = request.POST.get('pdt-name')
        desp = request.POST.get('pdt-desp')
        cate = request.POST.get('pdt-category')
        img = request.FILES.get('pdt-image')
        # print("IMG IMG IMG IMG !!!!:",img)
        if price != "" and name != "":
            flag +=1

        if re.search(priceOK,price) != None:
            flag += 1
            price = float(price)

        if flag == 2 :
            if desp != "":
                if img != "" :

                    pdt = Product.objects.create(name=name, is_available=True, descriptions=desp, price=price,
                                                 user=user, category=cate, image=img)
                else:
                    pdt = Product.objects.create(name=name, is_available=True, descriptions=desp,price=price,user=user,category=cate)
                pdt.save()
                print("IMG",img)
                return redirect('/')
            else :
                if img != "":
                    pdt = Product.objects.create(name=name, is_available=True, price=price, user=user, category=cate,image=img)
                else:
                    pdt = Product.objects.create(name=name, is_available=True, price=price, user=user, category=cate)
                pdt.save()
                print("IMG", img)
                return redirect('/')
        context = {
            'state' : "Check votre entrée !"
        }
    return render(request, 'deposer1.html', context)


@require_http_methods(['POST','GET'])
def detail_one(request,product_id):
    try:
        context = {}
        product = Product.objects.get(id=product_id)
        # print(product.__str__())
        username = request.session['username']
        user = User.objects.get(username=username)
        if request.method == "GET":
            pass
        if request.method == "POST":
            if request.POST['action'] == 'like':
                like = Like.objects.create(user=user,product=product)
                like.save()
                return redirect('/')
            if request.POST['action']== 'buy':
                if product.is_available == True:
                    product.is_available = False
                    product.save()
                    buy = Order.objects.create(status=TransactionStatus.INITIATED, buyer=user, product=product)
                    buy.save()
                else:
                    return HttpResponse("Désolée!\nLe produit est déjà achetée par quelqu'un autre!")
                return redirect('/')
        context = {'product': product,
                   'username':username,}
        return render(request,'productDetail.html', context)
    except DatabaseError as e:
        logging.warning(e)


def search_product(request):
    pass

def cooking_products(request):
    context = {}
    try:
        username = request.session['username']
        context['username'] =  username
        if request.method == "GET":
            pass
        if request.method == "POST":
            pass
    except KeyError as e:
        print("Nobody has been login.")
    return render(request,'pdt_cooking.html',context)


class CookingListView(generic.ListView):
    model = Product
    template_name = 'pdt_cooking.html'
    context_object_name = 'all'
    paginate_by = 5

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(CookingListView, self).get_context_data(*kwargs)
        paginator = context.get('paginator')
        page_obj = context.get('page_obj')
        pagination_data = self.get_pagination_data(paginator, page_obj)
        context.update(pagination_data)
        username=""
        try :
            username = self.request.session['username']
            context.update({'username':username})
        except KeyError as e:
            print("Nobody has been login.")
        return context

    def get_queryset(self):
        products = Product.objects.filter(Q(category__icontains="cook") | Q(category__icontains="cuis" )| Q(category__icontains="eat")).order_by('-date_post')
        return products


    def get_pagination_data(self, paginator, page_obj, around_count=2):
        currentpage = page_obj.number
        totalpage = paginator.num_pages
        left_has_more = False
        right_has_more = False
        if currentpage <= around_count + 2:
            leftpage = range(1, currentpage)
        else:
            left_has_more = True
            leftpage = range(currentpage - around_count, currentpage)

        if currentpage >= totalpage - around_count - 1:
            rightpage = range(currentpage + 1, totalpage)
        else:
            right_has_more = True
            rightpage = range(currentpage + 1, currentpage + around_count+1)

        return {
            'current_page' : currentpage,
            'left_pages': leftpage,
            'right_pages': rightpage,
            'left_has_more':left_has_more,
            'right_has_more':right_has_more,
            'total_page' :totalpage,
            'paginate_by':self.paginate_by,
        }

class SportListView(generic.ListView):
    model = Product
    template_name = 'pdt_sport.html'
    context_object_name = 'all'
    paginate_by = 5

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(SportListView, self).get_context_data(*kwargs)
        paginator = context.get('paginator')
        page_obj = context.get('page_obj')
        pagination_data = self.get_pagination_data(paginator, page_obj)
        context.update(pagination_data)
        username=""
        try :
            username = self.request.session['username']
            context.update({'username':username})
        except KeyError as e:
            print("Nobody has been login.")
        return context

    def get_queryset(self):
        products = Product.objects.filter(Q(category__icontains="sport") | Q(category__icontains="ball" )| Q(category__icontains="basket")| Q(category__icontains="swim")| Q(category__icontains="nage")).order_by('-date_post')
        return products


    def get_pagination_data(self, paginator, page_obj, around_count=2):
        currentpage = page_obj.number
        totalpage = paginator.num_pages
        left_has_more = False
        right_has_more = False
        if currentpage <= around_count + 2:
            leftpage = range(1, currentpage)
        else:
            left_has_more = True
            leftpage = range(currentpage - around_count, currentpage)

        if currentpage >= totalpage - around_count - 1:
            rightpage = range(currentpage + 1, totalpage)
        else:
            right_has_more = True
            rightpage = range(currentpage + 1, currentpage + around_count+1)

        return {
            'current_page' : currentpage,
            'left_pages': leftpage,
            'right_pages': rightpage,
            'left_has_more':left_has_more,
            'right_has_more':right_has_more,
            'total_page' :totalpage,
            'paginate_by':self.paginate_by,
        }

class AnimalListView(generic.ListView):
    model = Product
    template_name = 'pdt_animal.html'
    context_object_name = 'all'
    paginate_by = 5

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(AnimalListView, self).get_context_data(*kwargs)
        paginator = context.get('paginator')
        page_obj = context.get('page_obj')
        pagination_data = self.get_pagination_data(paginator, page_obj)
        context.update(pagination_data)
        username=""
        try :
            username = self.request.session['username']
            context.update({'username':username})
        except KeyError as e:
            print("Nobody has been login.")
        return context

    def get_queryset(self):
        products = Product.objects.filter(Q(category__icontains="animal") | Q(category__icontains="dog" )| Q(category__icontains="cat")| Q(category__icontains="chat")| Q(category__icontains="chien")).order_by('-date_post')
        return products


    def get_pagination_data(self, paginator, page_obj, around_count=2):
        currentpage = page_obj.number
        totalpage = paginator.num_pages
        left_has_more = False
        right_has_more = False
        if currentpage <= around_count + 2:
            leftpage = range(1, currentpage)
        else:
            left_has_more = True
            leftpage = range(currentpage - around_count, currentpage)

        if currentpage >= totalpage - around_count - 1:
            rightpage = range(currentpage + 1, totalpage)
        else:
            right_has_more = True
            rightpage = range(currentpage + 1, currentpage + around_count+1)

        return {
            'current_page' : currentpage,
            'left_pages': leftpage,
            'right_pages': rightpage,
            'left_has_more':left_has_more,
            'right_has_more':right_has_more,
            'total_page' :totalpage,
            'paginate_by':self.paginate_by,
        }

class StudyListView(generic.ListView):
    model = Product
    template_name = 'pdt_study.html'
    context_object_name = 'all'
    paginate_by = 5

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(StudyListView, self).get_context_data(*kwargs)
        paginator = context.get('paginator')
        page_obj = context.get('page_obj')
        pagination_data = self.get_pagination_data(paginator, page_obj)
        context.update(pagination_data)
        username=""
        key=""
        try :
            username = self.request.session['username']
            key = self.request
            context.update({'username':username})
        except KeyError as e:
            print("Nobody has been login.")
        return context

    def get_queryset(self):
        products = Product.objects.filter(Q(category__icontains="livre") | Q(category__icontains="book" )| Q(category__icontains="etu")| Q(category__icontains="text")| Q(category__icontains="nage")).order_by('-date_post')
        return products


    def get_pagination_data(self, paginator, page_obj, around_count=2):
        currentpage = page_obj.number
        totalpage = paginator.num_pages
        left_has_more = False
        right_has_more = False
        if currentpage <= around_count + 2:
            leftpage = range(1, currentpage)
        else:
            left_has_more = True
            leftpage = range(currentpage - around_count, currentpage)

        if currentpage >= totalpage - around_count - 1:
            rightpage = range(currentpage + 1, totalpage)
        else:
            right_has_more = True
            rightpage = range(currentpage + 1, currentpage + around_count+1)

        return {
            'current_page' : currentpage,
            'left_pages': leftpage,
            'right_pages': rightpage,
            'left_has_more':left_has_more,
            'right_has_more':right_has_more,
            'total_page' :totalpage,
            'paginate_by':self.paginate_by,
        }

class SearchListView(generic.ListView):
    model = Product
    template_name = 'pdt_search.html'
    context_object_name = 'all'
    paginate_by = 5

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(SearchListView, self).get_context_data(*kwargs)
        paginator = context.get('paginator')
        page_obj = context.get('page_obj')
        pagination_data = self.get_pagination_data(paginator, page_obj)
        context.update(pagination_data)
        username=""
        try :
            username = self.request.session['username']
            context.update({'username':username})
        except KeyError as e:
            print("Nobody has been login.")
        return context

    def get_queryset(self):
        products = Product.objects.filter(Q(category__icontains="livre") | Q(category__icontains="book" )| Q(category__icontains="etu")| Q(category__icontains="text")| Q(category__icontains="nage")).order_by('-date_post')
        return products


    def get_pagination_data(self, paginator, page_obj, around_count=2):
        currentpage = page_obj.number
        totalpage = paginator.num_pages
        left_has_more = False
        right_has_more = False
        if currentpage <= around_count + 2:
            leftpage = range(1, currentpage)
        else:
            left_has_more = True
            leftpage = range(currentpage - around_count, currentpage)

        if currentpage >= totalpage - around_count - 1:
            rightpage = range(currentpage + 1, totalpage)
        else:
            right_has_more = True
            rightpage = range(currentpage + 1, currentpage + around_count+1)

        return {
            'current_page' : currentpage,
            'left_pages': leftpage,
            'right_pages': rightpage,
            'left_has_more':left_has_more,
            'right_has_more':right_has_more,
            'total_page' :totalpage,
            'paginate_by':self.paginate_by,
        }


def bulk_create(request):
    list = []
    for i in range(10):
        movie_list = ['The Godfather', 'The Wizard of Oz', 'Citizen Kane', 'The Shawshank Redemption', 'Pulp Fiction']
        name = random.choice(movie_list)+"_book_{0}".format(i)
        price = random.randint(5,10)
        id = random.randint(1,3)
        user = User.objects.get(id=id)
        str1 ="I have become my own living proof that you need very little to be extremely happy."
        str2 = "The challenge of isolation in bad weather is boredom, particularly when your inside space is less than six square metres. "
        str3 = "Simple daily rhythms bring enormous comfort."
        desp = random.choice([str1,str2,str3]);
        cate = 'study'
        pdt = Product(name=name, is_available=True, descriptions=desp,price=price,user=user,category=cate)
        list.append(pdt)
    Product.objects.bulk_create(list);
    return HttpResponse("yes go!!!")