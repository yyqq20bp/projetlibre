from django.test import TestCase
from django.test import TestCase

from apps.favorite.models import Like
from .models import Product
from apps.users.models import User,Address
from apps.orders.models import *
# Create your tests here.

class ProductTest(TestCase):
    def setUp(self):
        add = Address.objects.create(zipcode=123456,city="LALA",street="aaaa",additional="assdfdsfsdf")
        user = User.objects.create_user(username="username1", password="password", email="string@mail.com", sex="Female",phone="123456789",is_superuser=0,address=add)
        pro = Product.objects.create(name='hahatest123456',is_available=1,category="aaaa",image="",descriptions="dasdasd",date_post="2020-03-30 16:24:04.762199",price="6.0",user = user)

    # tester /product/ GET
    # Accessible normalement, retournez STATUS_CODE=200
    def test_index_get(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    # tester /product/detail_one GET
    # Accessible normalement, retournez STATUS_CODE=301
    def test_detail_one_get(self):
        user = User.objects.get(username="username1")
        pro = Product.objects.create(name='hahatest', is_available=1, category="aaaa", image="",
                                     descriptions="dasdasd", date_post="2020-03-30 16:24:04.762199", price="6.0",
                                     user=user)
        str = '/'+pro.id.__str__()
        response = self.client.get(str)
        self.assertEqual(response.status_code, 301)

    # tester /product/add_product GET
    # Accessible normalement, retournez STATUS_CODE=200
    def test_add_product_get(self):
        response = self.client.get('/add_product')
        self.assertEqual(response.status_code, 200)

    # tester /product/cooking GET
    # Accessible normalement, retournez STATUS_CODE=200
    def test_cooking_get(self):
        response = self.client.get('/cooking')
        self.assertEqual(response.status_code, 200)

    # tester /product/sport GET
    # Accessible normalement, retournez STATUS_CODE=200
    def test_sport_get(self):
        response = self.client.get('/sport')
        self.assertEqual(response.status_code, 200)

    # tester /product/animal GET
    # Accessible normalement, retournez STATUS_CODE=200
    def test_animal_get(self):
        response = self.client.get('/animal')
        self.assertEqual(response.status_code, 200)

    # tester /product/study GET
    # Accessible normalement, retournez STATUS_CODE=200
    def test_study_get(self):
        response = self.client.get('/study')
        self.assertEqual(response.status_code, 200)

    def test_add_product_post(self):
        session = self.client.session
        session['username'] = "username1"
        session.save()
        user = User.objects.get(username="username1")
        reponse = self.client.post('/add_product', {'pdt-price': 20,
                                                    'pdt-name':'pdt-name-test',
                                                    'pdt-desp':'goodg good good',
                                                    'pdt-category':'others',
                                                     })
        self.assertEqual(reponse.status_code,302)
        pdt = Product.objects.get(name='pdt-name-test')
        self.assertTrue(user is not None)

    def test_detail_one_post_like(self):
        session = self.client.session
        session['username'] = "username1"
        session['action'] = "like"
        session.save()
        user = User.objects.get(username="username1")
        pro = Product.objects.create(name='hahatest', is_available=1, category="aaaa", image="",
                                     descriptions="dasdasd", date_post="2020-03-30 16:24:04.762199", price="6.0",
                                     user=user)
        str = '/' + pro.id.__str__()
        like = Like.objects.create(user=user,product=pro)
        reponse = self.client.post(str,)
        self.assertEqual(reponse.status_code, 301)
        like = Like.objects.get(pk=like.id)
        self.assertTrue(like is not None)


    def test_detail_one_post_buy(self):
        add1 = Address.objects.create(zipcode=123456, city="BBBBLALA", street="SSSaaaa", additional="assdfdsfsdfSSS")
        user2 = User.objects.create_user(username="username2", password="password", email="string@mail.com",
                                        sex="Female", phone="123456789", is_superuser=0, address=add1)
        session = self.client.session
        session['username'] = "username2"
        session['action'] = "buy"
        session.save()
        pro = Product.objects.filter()[:1].get()
        buy = Order.objects.create(status=TransactionStatus.INITIATED,buyer=user2,product=pro)
        str = '/' + pro.id.__str__()
        reponse = self.client.post(str, )
        self.assertEqual(reponse.status_code, 301)
        buy= Order.objects.get(pk=buy.id)
        self.assertTrue(buy is not None)







