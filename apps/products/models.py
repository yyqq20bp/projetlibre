from django.db import models
from apps.users.models import User
from datetime import date
from django.utils import timezone
# Create your models here.
class Product(models.Model):
    name = models.CharField(max_length=32)
    is_available = models.BooleanField(default=True)
    category = models.CharField(max_length=16)
    image = models.ImageField(upload_to='upload/products/', null=True)
    descriptions = models.CharField(max_length=64,null=True)
    date_post = models.DateTimeField(default=timezone.now)
    price = models.FloatField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
    # m = models.ForeignKey(Address, on_delete=models.CASCADE)
