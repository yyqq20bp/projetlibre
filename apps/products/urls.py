from django.conf.urls import url
from django.urls import path
from .views import *

urlpatterns = [
    path('', IndexView.as_view(), name='products'),
    path("<int:product_id>/",  detail_one, name='detail'),
    path("add_product",add_product, name='add_product'),
    path("cooking",CookingListView.as_view(),name='cooking_products'),
    path("sport",SportListView.as_view(),name='sport_products'),
    path("animal",AnimalListView.as_view(),name='animal_products'),
    path("study",StudyListView.as_view(),name='study_products'),
    path("search/<slug:key>",SearchListView.as_view(),name='search_products'),
    path("test_bulk_create",bulk_create,name='bulk_create'),
    # path("")
]