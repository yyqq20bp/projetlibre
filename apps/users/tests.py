from django.test import TestCase
from .models import User, Address
from django.contrib import auth
from django.conf import settings
from importlib import import_module

# Create your tests here.
class SessionTestCase(TestCase):
    def setUp(self):
        settings.SESSION_ENGINE = 'django.contrib.sessions.backends.cached_db'
        engine = import_module(settings.SESSION_ENGINE)
        store = engine.SessionStore()
        store.save()
        self.session = store
        self.client.cookies[settings.SESSION_COOKIE_NAME] = store.session_key

class UserTest(TestCase):


    def setUp(self):
        add = Address.objects.create(zipcode=123456,city="LALA",street="aaaa",additional="assdfdsfsdf")
        User.objects.create_user(username="username", password="password", email="string@mail.com", sex="Female",phone="123456789",is_superuser=0,address=add)

    # tester /user/login GET
    # Accessible normalement, retournez STATUS_CODE=200
    def test_login_get(self):
        response = self.client.get('/user/login')
        self.assertEqual(response.status_code, 200)

    # tester /user/login POST
    # Entrez un nom d'utilisateur et un mot de passe valides et retournez STATUS_CODE=302
    def test_login_post_ok(self):
        response = self.client.post('/user/login', {'username': 'username', 'password': 'password'})
        self.assertEqual(response.status_code, 302)

    # Entrez un nom d'utilisateur valide et un mot de passe invalide, et revenez à la personnalisation STATUS_CODE=205
    def test_login_post_pwd_wrong(self):
        response = self.client.post('/user/login', {'username': 'username', 'password': '123'})
        self.assertEqual(response.status_code, 205)

    # Entrez un nom d'utilisateur non valide, personnaliser retourner STATUS_CODE=204
    def test_login_post_username_wrong(self):
        response = self.client.post('/user/login', {'username': '不存在', 'password': '123'})
        self.assertEqual(response.status_code, 204)

    # Tester /user/verify GET
    # Accessible normalement, retournez STATUS_CODE=200
    def test_verify_get(self):
        response = self.client.get('/user/verify')
        self.assertEqual(response.status_code, 200)

    # Tester /user/verify POST
    # Lorsque le format de la boîte aux lettres est correct, passez à la page suivante en douceur et revenez STATUS_CODE=302
    def test_verify_post_ok(self):
        response = self.client.post('/user/verify', {'email': 'string@testmail.com'})
        self.assertEqual(response.status_code, 302)

    # Lorsque le format de la boîte aux lettres est incorrect，personnaliser retourner STATUS_CODE=204
    def test_verify_post_wrong(self):
        response = self.client.post('/user/verify', {'email': 'hahahahah'})
        self.assertEqual(response.status_code, 204)

    # tester /user/register GET
    # Accessible normalement, retournez STATUS_CODE=200
    def test_register_get(self):
        response = self.client.get('/user/register')
        self.assertEqual(response.status_code, 200)

    # tester /user/register POST ，En même temps, testez s'il peut insérer avec succès un nouvel utilisateur dans la base de données
    # Lorsque le format de l'e-mail est correct, passez à la page d'accueil et retournez STATUS_CODE=302
    # Insérer l'utilisateur avec succès, le résultat de la requête n'est pas NONE
    def test_register_post_ok(self):
        print("SESSION:",self.client.session)
        session = self.client.session
        session['rg-code'] = '12Ab9R'
        session['rg-email'] = 'string@testmail.com'
        session.save()
        response = self.client.post('/user/register', {'email': 'string@testmail.com',
                                                       'password':'password',
                                                       'repeat_password':'password',
                                                       'username':'testusername8899',
                                                       'firstname':'firstname',
                                                       'lastname':'lastname',
                                                       'sex':'Female',
                                                       'rgemailcode':'12Ab9R',
                                                       })
        self.assertEqual(response.status_code, 302)
        user = User.objects.get(username="testusername8899")
        self.assertTrue(user is not None)

    # tester /user/register POST
    # Entrez le code de vérification est incorrect, personnaliser retourner STATUS_CODE=204
    def test_register_post_wrong(self):
        session = self.client.session
        session['rg-code'] = '12Ab9R'
        session['rg-email'] = 'string@testmail.com'
        session.save()
        response = self.client.post('/user/register', {'email': 'string@testmail.com',
                                                       'password':'password',
                                                       'repeat_password':'password',
                                                       'username':'testusername8899',
                                                       'firstname':'firstname',
                                                       'lastname':'lastname',
                                                       'sex':'Female',
                                                       'rgemailcode':'?????',
                                                       })
        self.assertEqual(response.status_code, 204)

    # Tester /user/edit_phone GET
    # Accessible normalement, retournez STATUS_CODE=200
    def test_edit_phone_get(self):
        response = self.client.get('/user/edit_phone')
        self.assertEqual(response.status_code, 200)

    # Tester /user/edit_phone POST ，Dans le même temps, vérifiez si la modification du numéro de téléphone dans la base de données est réussie
    # Une fois la modification réussie, retournez à la page d'accueil et retournez STATUS_CODE=302
    # Recherchez le résultat modifié dans la base de données, conformément au contenu modifié
    def test_edit_phone_post_ok(self):
        session = self.client.session
        session['username'] = 'username'
        session.save()
        response = self.client.post('/user/edit_phone', {'phone':'8833229988'})
        self.assertEqual(response.status_code, 302)
        user = User.objects.get(username="username")
        self.assertEqual(user.phone,8833229988)

    # Tester /user/edit_name GET
    # Accessible normalement, retournez STATUS_CODE=200
    def test_edit_name_get(self):
        response = self.client.get('/user/edit_name')
        self.assertEqual(response.status_code, 200)

    # Tester /user/edit_name POST ，Dans le même temps, vérifiez si la modification du nom dans la base de données est réussie
    # Une fois la modification réussie, retournez à la page d'accueil et retournez STATUS_CODE=302
    # Recherchez le résultat modifié dans la base de données, conformément au contenu modifié
    def test_edit_name_post_ok(self):
        session = self.client.session
        session['username'] = 'username'
        session.save()
        response = self.client.post('/user/edit_name', {'firstname':'hahaha','lastname':'yoyoyo'})
        self.assertEqual(response.status_code, 302)
        user = User.objects.get(username="username")
        self.assertEqual(user.first_name,"hahaha")
        self.assertEqual(user.last_name,"yoyoyo")

    # Tester /user/edit_pwd GET
    # Accessible normalement, retournez STATUS_CODE=200
    def test_edit_pwd_get(self):
        response = self.client.get('/user/edit_pwd')
        self.assertEqual(response.status_code, 200)

    # Tester /user/edit_pwd POST ，Dans le même temps, vérifiez si le changement de mot de passe dans la base de données a réussi
    # Lorsque le format de l'e-mail est correct, retournez à la page d'accueil et retournez STATUS_CODE=302
    # Modifiez le mot de passe, le résultat de la requête n'est pas NONE
    def test_edit_pwd_post_ok(self):
        session = self.client.session
        session['username'] = 'username'
        session.save()
        response = self.client.post('/user/edit_pwd', {'old-pwd':'password','new-pwd':'newpassword','re-pwd':'newpassword'})
        self.assertEqual(response.status_code, 302)
        user = auth.authenticate(username='username', password='newpassword')
        self.assertTrue(user is not None)

    # Tester /user/edit_pwd POST
    # Entrez l'ancien mot de passe est incorrect, personnaliser retourner STATUS_CODE=204
    def test_edit_pwd_post_wrong(self):
        session = self.client.session
        session['username'] = 'username'
        session.save()
        response = self.client.post('/user/edit_pwd',
                                {'old-pwd': 'nononono', 'new-pwd': 'newpassword', 're-pwd': 'newpassword'})
        self.assertEqual(response.status_code, 204)


    # Tester /user/edit_address GET
    # Accessible normalement, retournez STATUS_CODE=200
    def test_edit_address_get(self):
        response = self.client.get('/user/edit_address')
        self.assertEqual(response.status_code, 200)


    # Tester /user/edit_address POST ，Vérifiez en même temps si la nouvelle adresse est correctement insérée dans la base de données
    # Modifiez l'adresse avec succès, revenez à la page d'accueil, retournez STATUS_CODE=302
    # La base de données a réussi à insérer une nouvelle adresse, le résultat de la requête n'est pas NONE
    def test_edit_address_post_ok(self):
        session = self.client.session
        session['username'] = 'username'
        session.save()
        response = self.client.post('/user/edit_address', {'city':'cccity','zipcode':'987654','street':'dreamstreet','addition':'appt 00000'})
        self.assertEqual(response.status_code, 302)
        user = User.objects.get(username="username")
        add = user.address
        self.assertEqual(add.city,"cccity")
        self.assertEqual(add.zipcode, 987654)







