
from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
# This class is to describe the address
class Address(models.Model):
    # id = models.AutoField(primary_key=True)
    zipcode = models.IntegerField()
    city = models.CharField(max_length=64)
    street = models.CharField(max_length=128)
    additional = models.TextField(max_length=128, blank=True)

    def __str__(self):
        return '%s, %s, %s' % (self.city, self.street, self.additional)


# This class is to describe the user of this system, there are two types of user, administrator and common client
# The administrator can take charge of the client and all of their activities
# The client can use this system to do their transaction
class User(AbstractUser):
    # id = models.AutoField(primary_key=True)
    _SEX = (
        ('F', 'Female'),
        ('M', 'Male'),
    )
    phone = models.IntegerField(max_length=16,blank=True)
    sex = models.CharField(max_length=1, blank=True)
    address = models.ForeignKey(Address, on_delete=models.CASCADE, blank=True)

    class Meta(AbstractUser.Meta):
        pass

    def __str__(self):
        return self.email