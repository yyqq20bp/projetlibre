
from myproject import settings
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.contrib import auth
from django.urls import reverse
from django.db import DatabaseError
from django.shortcuts import render, redirect
from django.views.decorators.http import require_http_methods
from django.contrib.auth.decorators import login_required
import re
from .models import *
import random
from django.core.mail import EmailMultiAlternatives

# Create your views here.

# /user/login
@require_http_methods(['POST','GET'])
def login(request):
    """
    this function is to present the existed user's login operation
    :param request: user's input
    :return: the index of app or redirect this page if password or email is wrong
    """
    context = {}
    if request.method == "GET":
        return render(request, "login.html", context, status=200)
    else :
        messages.error(request, '用户名或密码不正确')
        username = request.POST.get('username')
        print("username",username)
        password = request.POST.get('password')
        user = auth.authenticate(username=username, password=password)
        try:
            is_exist = User.objects.get(username=username)
            request.session.clear()
            if is_exist is not None:
                if user is not None:
                    auth.login(request, user)
                    context = {'username': username}
                    request.session['username'] = username
                    return redirect('/')
                else:
                    context = {'info': 'Check votre mots de passe.'}
                    return render(request, "login.html", context, status=205)


        except ObjectDoesNotExist as e:
            print(e.__str__())
            context = {'info': 'Ce compte n\'existe pas.'}
        return render(request, "login.html", context=context, status=204)

@login_required
def login_out(request):
    auth.logout(request)
    request.session.clear()
    return redirect('/')


def add_ads(request):
    pass


def user_ads(request):
    # 获取用户id（session中获取）
    # 查询数据库中Address表中该用户id的数据
    # 返回页面
    user_id = request.session.get('user_id')
    adss = Address.objects.filter(user_id=user_id)
    content = {"adss": adss}
    return render(request, '', content)


def delete_ads(request):
    # 删除地址功能
    # 获取用户id
    # 获取地址id
    # 查询该数据
    # 删除该数据
    # 返回页面
    user_id = request.session.get('user_id')
    adsid = request.GET.get('adsid')
    try:
        delads = Address.objects.get(id=adsid, user_id=user_id)
        delads.delete()
    except DatabaseError as e:
        pass
    return redirect('/')

def create_verify_code():
    n1 = random.randint(0,9)
    n2 = random.randint(0,9)
    n3 = random.choice('QAZWSXEDCRFVTGBYHNUJMIKOLP')
    n4 = random.choice('qazwsxedcrfvtgbyhnujmikolp')
    n5 = random.choice('QAZWSXEDCRFVTGBYHNUJMIKOLPqazwsxedcrfvtgbyhnujmikolp1234567890')
    n6 = random.choice('QAZWSXEDCRFVTGBYHNUJMIKOLPqazwsxedcrfvtgbyhnujmikolp1234567890')
    str  = n1.__str__() + n2.__str__()+ n3 + n4 + n5.__str__() + n6.__str__()
    return str

def send_email(to_email,code):
    subject = 'Verify your email'
    text_content = 'Hello, \n Here is your code {0}.\n Please remember it and fill your inscription form with it.\nThank you!'.format(code)
    html_content = '<p>Hello,</p> ' \
                   '<p>Here is your code <strong>{0}</strong>.</p> ' \
                   '<p>Please remember it and fill your inscription form with it.</p> ' \
                   '<p>Thank you!</p> ' .format(code)
    msg = EmailMultiAlternatives(subject, text_content, settings.EMAIL_HOST_USER, [to_email])
    msg.attach_alternative(html_content, "text/html")
    msg.send()

# /user/register
@require_http_methods(['POST','GET'])
def verify_email(request):
    context = {}
    if request.method == "GET":
        return render(request, 'registerEmail.html', context, status=200)
    if request.method == "POST":
        flag = 0
        code = create_verify_code()
        print("MAIL CODE : ",code)
        email = request.POST.get('email')
        regex = '^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'
        if email != "" and re.search(regex, email) is not None:
            flag += 1
            print("email : " ,email)
        if flag == 1:
            send_email(email,code)
            request.session['rg-email'] = email
            request.session['rg-code'] = code
            return redirect('/user/register')
        return render(request, 'registerEmail.html', context=context,status=204)


# /user/register
@require_http_methods(['POST','GET'])
def register(request):
    """
    this function is to present the user's inscription
    it will get user's input information, judge whether the user exists or not, finish inscription operation
    :param request: user's input
    :return: the index of app or redirect this page
    """
    context = {}
    if request.method == "GET":
        return render(request, 'register.html', context=context,status=200)
    if request.method == "POST":
        state = ""
        flag  = 0
        email = request.POST.get('email')
        password = request.POST.get('password')
        repeat_password = request.POST.get('repeat_password')
        username = request.POST.get('username')
        firstname = request.POST.get('firstname')
        lastname = request.POST.get('lastname')
        sex = request.POST.get('sex')
        code = request.POST.get('rgemailcode')
        rg_email=request.session.get('rg-email')
        print("session email:", rg_email)
        rg_code = request.session.get('rg-code')
        print("session code:",rg_code)
        add = Address.objects.get(pk=1)
        if User.objects.filter(username=username):
            state = 'Ce compte de USERNAME a été enregistré pour utilisation! '
            context = {
                'state': state,
            }
            return render(request, 'register.html', context)
        else:
            flag += 1
            print("flag user: " + flag.__str__())

        if email != "" and password != "" and repeat_password != "" and username != "" and sex != "":
            flag += 1
            print("flag vide: " + flag.__str__())

        if email == rg_email:
            flag += 1
            print("flag mail: " + flag.__str__())

        if password == repeat_password :
            flag += 1
            print("flag pwd?: " + flag.__str__())

        if len(password) >= 8 :
            flag += 1
            print("flag lenpwd: " + flag.__str__())

        if code == rg_code:
            flag += 1
            print("flag code: ",code)

        if flag == 6 :
            if firstname != None and lastname != None:
                new_user = User.objects.create_user(username=username, password=password, email=email,
                                                    first_name=firstname, last_name=lastname, sex=sex,phone="0",is_superuser=0,address=add)
                new_user.save()
            elif firstname == None and lastname != None:
                new_user = User.objects.create_user(username=username, password=password, email=email,
                                                    last_name=lastname, sex=sex,phone="0",is_superuser=0,address=add)
                new_user.save()
            elif firstname != None and lastname == None:
                new_user = User.objects.create_user(username=username, password=password, email=email,
                                                    first_name=firstname, sex=sex,phone="0",is_superuser=0,address=add)
                new_user.save()
            else:
                new_user = User.objects.create_user(username=username, password=password, email=email, sex=sex,phone="0",is_superuser=0,address=add)
                new_user.save()
            context = {
                'username': username,
                'state' : "Vouz avez bien été enregistré(e) !"
            }
            request.session.clear()
            return redirect('/')
        context = {
        'state': "Check votre entrée !"
        }
        return render(request, 'register.html', context=context, status=204)


@require_http_methods(['POST','GET'])
def edit_profile(request):
    context = {}
    val = ""
    if request.method == "GET":
        pass
    if request.method == "POST":
        # password = request.POST.get('pwd')
        # repeat_password = request.POST.get('re-pwd')
        # firstname = request.POST.get('firstname')
        # lastname = request.POST.get('lastname')
        # sex = request.POST.get('sex')
        # city = request.POST.get('city')
        val = request.POST.get('submit-type')
    context = {
            'state': "Check votre entrée !"
    }
    return render(request,'editProfile.html',context)

@require_http_methods(['POST','GET'])
def edit_name(request):
    context = {}
    val = ""
    if request.method == "GET":
        pass
    if request.method == "POST":
        firstname = request.POST.get('firstname')
        lastname = request.POST.get('lastname')
        username = request.session['username']
        user = User.objects.get(username=username)
        if firstname is not None and lastname is not None :
            user.first_name = firstname
            user.last_name = lastname
            user.save()
            return redirect('/')

    context = {
            'state': "Check votre entrée !"
    }
    return render(request,'edit_name.html',context)

@require_http_methods(['POST','GET'])
def edit_pwd(request):
    context = {}
    if request.method == "GET":
        context = { 'state': ""}
        return render(request, 'edit_pwd.html', context,status=200)
    if request.method == "POST":
        username = request.session['username']
        old_password = request.POST.get('old-pwd')
        user = auth.authenticate(username=username, password=old_password)
        password = request.POST.get('new-pwd')
        repeat_password = request.POST.get('re-pwd')
        if user is not None and password is not None and repeat_password is not None:
            if password == repeat_password:
                user.set_password(password)
                user.save()
                auth.logout(request)
                return redirect(reverse('login'))
        context = {
            'state': "Check votre entrée !"
        }
        return render(request,'edit_pwd.html',context,status=204)

@require_http_methods(['POST','GET'])
def edit_address(request):
    context = {}
    if request.method == "GET":
        pass
    if request.method == "POST":
        city = request.POST.get('city')
        zipcode = request.POST.get('zipcode')
        street = request.POST.get('street')
        addi = request.POST.get('addition')
        username = request.session['username']
        user = User.objects.get(username=username)
        zipcheck ='^[0-9]*$'
        if city is not None and zipcode is not None and street is not None:
            if re.search(zipcheck,zipcode) != None:
                add = None
                if addi is not None:
                    add = Address.objects.create(zipcode=zipcode, street=street, city=city, additional=addi)
                else:
                    add = Address.objects.create(zipcode=zipcode, street=street, city=city)
                user.address = add
                user.save()
                return redirect('/')
    context = {
            'state': "Check votre entrée !"
    }
    return render(request,'edit_address.html',context)

@require_http_methods(['POST','GET'])
def edit_phone(request):
    context = {}
    val = ""
    if request.method == "GET":
        pass
    if request.method == "POST":
        phone = request.POST.get('phone')
        username = request.session['username']
        user = User.objects.get(username=username)
        if phone is not None and user is not None :
            user.phone = phone
            user.save()
            return redirect('/')
    context = {
            'state': "Check votre entrée !"
    }
    return render(request,'edit_phone.html',context)





