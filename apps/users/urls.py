from django.conf.urls import url
from django.urls import path
from .views import *

urlpatterns = [
    path('login', login, name='login'),
    path('verify',verify_email,name='verify_email'),
    path('register', register, name='register'),
    path('logout',login_out,name='logout'),
    path('edit_name',edit_name,name='edit_name'),
    path('edit_address',edit_address,name='edit_address'),
    path('edit_phone',edit_phone,name='edit_phone'),
    path('edit_pwd',edit_pwd,name='edit_pwd'),

]

