from django.views.decorators.http import require_http_methods, require_GET,require_POST
from django.http import HttpResponse
from django.shortcuts import render,redirect,reverse
from django.views import generic
from .models import *
import logging
from apps.orders.urls import *
from sqlite3 import DatabaseError
# Create your views here.
def all_likes(request):
    return HttpResponse("all your like!")


class AllLikesView(generic.ListView):
    model = Like
    template_name = 'allLikes.html'
    context_object_name = 'likes'
    paginate_by = 3
    username = ""
    user = None
    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(AllLikesView, self).get_context_data(*kwargs)
        paginator = context.get('paginator')
        page_obj = context.get('page_obj')
        pagination_data = self.get_pagination_data(paginator, page_obj)
        try:
            self.username = self.request.session.get('username')
            self.user = User.objects.get(username=self.username)
            self.request.session['username'] = self.username
        except KeyError as e:
            print("Nobody has been login.")
        context.update(pagination_data)
        return context

    def get_queryset(self):
        try :
            self.username = self.request.session.get('username')
            self.user = User.objects.get(username=self.username)
            likes = Like.objects.filter(user=self.user).order_by('-date_likes')
        except KeyError as e:
            print("Nobody has been login.")
        return likes

    def post(self, request, *args, **kwargs):
        like_id = request.POST.get('like-id')
        like  = Like.objects.get(pk=like_id)
        pdt_id = like.product.id
        if request.POST['action'] == 'dislike':
            cancel_one_like_url = reverse('cancel_one_like',kwargs={'like_id':like_id})
            return redirect(cancel_one_like_url)
        if request.POST['action'] == 'buy':
            go_to_buy_url = reverse('go_to_buy',kwargs={'like_id':like_id, 'product_id':pdt_id})
            return redirect(go_to_buy_url)

    def get_pagination_data(self, paginator, page_obj, around_count=2):
        currentpage = page_obj.number
        totalpage = paginator.num_pages
        left_has_more = False
        right_has_more = False
        if currentpage <= around_count + 2:
            leftpage = range(1, currentpage)
        else:
            left_has_more = True
            leftpage = range(currentpage - around_count, currentpage)

        if currentpage >= totalpage - around_count - 1:
            rightpage = range(currentpage + 1, totalpage)
        else:
            right_has_more = True
            rightpage = range(currentpage + 1, currentpage + around_count+1)

        return {
            'current_page' : currentpage,
            'left_pages': leftpage,
            'right_pages': rightpage,
            'left_has_more':left_has_more,
            'right_has_more':right_has_more,
            'total_page' :totalpage,
            'paginate_by':self.paginate_by,
            'username':self.username,
        }
@require_http_methods(['POST','GET'])
def cancel_one_like(request,like_id):
    try:
        context = {}
        like = Like.objects.get(pk=like_id)
        product = like.product
        username = like.user.username
        user = User.objects.get(username=username)
        if request.method == "GET":
            pass
        if request.method == "POST":
            if request.POST['action'] == 'yes':
                like.delete()
            if request.POST['action'] == 'no':
                pass
            listlike_url = reverse('all_likes', kwargs={'username':username})
            return redirect(listlike_url)
        context = {'product': product,
                   'username': username, }
        return render(request, 'cancel_one_like.html', context)

    except DatabaseError as e:
        logging.warning(e)


