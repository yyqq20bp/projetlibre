
from django.test import TestCase
from .models import Like
from apps.products.models import Product
from apps.users.models import User,Address
# Create your tests here.

class LikeTest(TestCase):

    def setUp(self):
        self.add = Address.objects.create(zipcode=123456,city="LALA",street="aaaa",additional="assdfdsfsdf")
        self.user = User.objects.create_user(username="username5", password="password", email="string@mail.com", sex="Female",phone="123456789",is_superuser=0,address=self.add)
        self.pro = Product.objects.create(name='123456',is_available=1,category="aaaa",image="",descriptions="dasdasd",date_post="2020-03-30 16:24:04.762199",price="6.0",user = self.user)

        self.like = Like.objects.create(user = self.user,product = self.pro)

    # tester /like/<username> GET
    # Accessible normalement, retournez STATUS_CODE=200
    def test_index_get(self):
        session = self.client.session
        session['username'] = "username5"
        session.save()
        str = '/like/'+ self.user.username
        response = self.client.get(str)
        self.assertEqual(response.status_code, 200)

    # tester '/like/<int:like_id>/cancel_like GET
    def test_cancel_one_like_get(self):
        session = self.client.session
        session['username'] = "username5"
        session.save()
        str = '/like/' + self.like.id.__str__() + '/cancel_like'
        response = self.client.get(str)
        self.assertEqual(response.status_code, 200)