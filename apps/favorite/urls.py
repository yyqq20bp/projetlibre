from django.conf.urls import url
from django.urls import path
from .views import *

urlpatterns = [
    # path('', IndexView.as_view(), name='products'),
    # path("<int:product_id>/",  detail_one, name='detail'),
    # path("add_product",add_product, name='add_product'),
    path('<username>',AllLikesView.as_view(),name='all_likes'),
    path('<int:like_id>/cancel_like',cancel_one_like, name='cancel_one_like')
]