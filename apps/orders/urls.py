from django.conf.urls import url
from django.urls import path
from .views import go_to_buy,AllOrdersView,delete_one_pdt,comment_one_pdt

urlpatterns = [
    path('<username>/',AllOrdersView.as_view(),name='all_orders'),
    path('<int:like_id>/<int:product_id>/go_to_buy', go_to_buy, name='go_to_buy'),
    path('<int:order_id>/<int:product_id>/delete_one_pdt', delete_one_pdt, name='delete_one_pdt'),
    path('<int:order_id>/comment', comment_one_pdt, name='comment_one_pdt')

]