from django.test import TestCase

from apps.favorite.models import Like
from apps.users.models import *
from apps.products.models import *
from .models import *
# Create your tests here.


class OrderTest(TestCase):

    def setUp(self):
        self.add= Address.objects.create(zipcode=123456,city="LALA",street="aaaa",additional="assdfdsfsdf")
        self.user = User.objects.create_user(username="username1", password="password", email="string@mail.com", sex="Female",phone="123456789",is_superuser=0,address=self.add)
        self.buyer = User.objects.create_user(username="username2", password="password", email="string@mail.com",
                                        sex="Female", phone="123456789", is_superuser=0, address=self.add)
        self.pro = Product.objects.create(name='123456',is_available=1,category="aaaa",image="",descriptions="dasdasd",date_post="2020-03-30 16:24:04.762199",price="6.0",user = self.user)

        self.like = Like.objects.create(user=self.buyer,product=self.pro)
        self.ord = Order.objects.create(status='INITIATED',buyer = self.buyer,product =self.pro)

    # tester /order/index GET
    # Accessible normalement, retournez STATUS_CODE=200
    def test_index_get(self):
        user = User.objects.get(username="username1")
        pro = Product.objects.create(name='hahatest', is_available=1, category="aaaa", image="",
                                     descriptions="dasdasd", date_post="2020-03-30 16:24:04.762199", price="6.0",
                                     user=user)

        ord = Order.objects.create(date_order='2020-03-30 16:24:04.762199',status='INITIATED',buyer = user,product = pro)

        str = '/order/'+ord.buyer.__str__()
        response = self.client.get(str)
        self.assertEqual(response.status_code, 301)


    # tester '<int:like_id>/<int:product_id>/go_to_buy' GET
    def test_go_to_buy_get(self):
            session = self.client.session
            session['username'] = "username2"
            session.save()
            str = '/order/' + self.like.id.__str__()+'/'+ self.pro.id.__str__()+'/go_to_buy'
            response = self.client.get(str)
            self.assertEqual(response.status_code,200)

    # tester '<int:order_id>/comment' GET
    def test_comment_one_pdt_get(self):
        session = self.client.session
        session['username'] = "username2"
        session.save()
        str = '/order/' + self.ord.id.__str__()+'/comment'
        response = self.client.get(str)
        self.assertEqual(response.status_code,200)

    # tester '<int:order_id>/<int:product_id>/delete_one_pdt' GET
    def test_delete_one_pdt_get(self):
        pass
        # session = self.client.session
        # session['username'] = "username2"
        # session.save()
        # str = '/order/' + self.ord.id.__str__()+ self.pro.id.__str__()+'/delete_one_pdt'
        # response = self.client.get(str)
        # self.assertEqual(response.status_code, 200)