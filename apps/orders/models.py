from django.db import models
from enum import Enum
from apps.products.models import *
from apps.users.models import *


# Create your models here.

class TransactionStatus(Enum):
    INITIATED = "INITIATED"
    PENDING = "PENDING"
    COMPLETED = "COMPLETED"
    FAILED = "FAILED"
    ERROR = "ERROR"


class Order(models.Model):
    date_order = models.DateTimeField(default=timezone.now)
    status = models.CharField(max_length=32)
    buyer = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)


class OrderComment(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    content = models.TextField(max_length=128)
    date_comment = models.DateTimeField(default=timezone.now)