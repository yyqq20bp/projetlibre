
from django.views import generic
from django.shortcuts import render,redirect,reverse
from apps.favorite.models import Like
from .models import *
from sqlite3 import DatabaseError
import logging
from apps.favorite.urls import *
from django.http import HttpResponse
# Create your views here.

class AllOrdersView(generic.ListView):
    model = Order
    template_name = 'allorders.html'
    context_object_name = 'orders'
    paginate_by = 3
    username = ""
    user = None

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(AllOrdersView, self).get_context_data(*kwargs)
        paginator = context.get('paginator')
        page_obj = context.get('page_obj')
        pagination_data = self.get_pagination_data(paginator, page_obj)
        context.update(pagination_data)
        return context

    def get_queryset(self):
        try :
            self.username = self.request.session['username']
            self.user = User.objects.get(username=self.username)
            orders = Order.objects.filter(buyer=self.user).order_by('-date_order')
        except KeyError as e:
            print("Nobody has been login.")
        return orders

    def post(self, request, *args, **kwargs):
        order_id = request.POST.get('order-id')
        order = Order.objects.get(pk=order_id)
        pdt = order.product
        buyer = order.buyer
        if request.POST['action'] == 'cancel':
            cancel_one_buy_url = reverse('delete_one_pdt',kwargs={'order_id':order_id, 'product_id':pdt.id})
            return redirect(cancel_one_buy_url)
        if request.POST['action'] == 'comment':
            comment_one_order_url = reverse('comment_one_pdt', kwargs={'order_id': order_id})
            return redirect(comment_one_order_url)


    def get_pagination_data(self, paginator, page_obj, around_count=2):
        currentpage = page_obj.number
        totalpage = paginator.num_pages
        left_has_more = False
        right_has_more = False
        if currentpage <= around_count + 2:
            leftpage = range(1, currentpage)
        else:
            left_has_more = True
            leftpage = range(currentpage - around_count, currentpage)

        if currentpage >= totalpage - around_count - 1:
            rightpage = range(currentpage + 1, totalpage)
        else:
            right_has_more = True
            rightpage = range(currentpage + 1, currentpage + around_count+1)

        return {
            'current_page' : currentpage,
            'left_pages': leftpage,
            'right_pages': rightpage,
            'left_has_more':left_has_more,
            'right_has_more':right_has_more,
            'total_page' :totalpage,
            'paginate_by':self.paginate_by,
            'username':self.username,
        }


def go_to_buy(request,like_id,product_id):
    try:
        context = {}
        like = Like.objects.get(pk=like_id)
        product = Product.objects.get(pk=product_id)
        username = like.user.username
        user = User.objects.get(username=username)
        if request.method == "GET":
            pass
        if request.method == "POST":
            if request.POST['action'] == 'yes':
                if  product.is_available == True:
                    buy = Order.objects.create(status=TransactionStatus.INITIATED, buyer=user, product=product)
                    buy.save()
                else:
                    return HttpResponse("Désolée!\nLe produit est déjà achetée par quelqu'un autre!")
            if request.POST['action'] == 'no':
                pass
            listlike_url = reverse('all_likes', kwargs={'username': username})
            return redirect(listlike_url)
        context = {'product': product,
                   'username': username, }
        return render(request, 'go_to_buy_one_pdt.html', context)

    except DatabaseError as e:
        logging.warning(e)


def delete_one_pdt(request,order_id,product_id):
    try:
        context = {}
        order = Order.objects.get(pk=order_id)
        product = order.product
        username = order.buyer.username
        user = User.objects.get(username=username)
        if request.method == "GET":
            pass
        if request.method == "POST":
            if request.POST['action'] == 'yes':
                order.delete()
                product.is_available == True
                product.save()
            if request.POST['action'] == 'no':
                pass
            listorder_url = reverse('all_orders', kwargs={'username':username})
            return redirect(listorder_url)
        context = {'product': product,
                   'username': username, }
        return render(request, 'delete_one_pdt.html', context)

    except DatabaseError as e:
        logging.warning(e)

def comment_one_pdt(request,order_id):
    try:
        context = {}
        order = Order.objects.get(pk=order_id)
        product = order.product
        username = order.buyer.username
        user = User.objects.get(username=username)
        if request.method == "GET":
            pass
        if request.method == "POST":
            if request.POST['action'] == 'yes':
                cmt = request.POST.get('cmt')
                if cmt is not None:
                    ordcmt = OrderComment.objects.create(order=order,content=cmt)
                    ordcmt.save()
            if request.POST['action'] == 'no':
                pass
            listorder_url = reverse('all_orders', kwargs={'username':username})
            return redirect(listorder_url)
        context = {'product': product,
                   'username': username, }
        return render(request, 'comment_one_order.html', context)

    except DatabaseError as e:
        logging.warning(e)